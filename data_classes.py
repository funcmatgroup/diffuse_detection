import numpy as np
import tensorflow as tf
from tensorflow import keras
import h5py
import os

"""
Class to allow iterative generation of 3D diffraction data

Modified from code found at https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
"""

class DataGenerator(keras.utils.Sequence):
    """Generates data for Keras"""
    
    def __init__(self,
                 list_IDs,
                 labels,
                 h5dir = '.',
                 normalize=True,
                 batch_size=32,
                 dim=(100,100,100),
                 n_channels=1,
                 n_classes=4,
                 shuffle=True
                 ):
        """ Initialise data sequence for diffraction voxels """
        self.dim = dim
        self.batch_size = batch_size
        self.labels = labels
        self.h5dir = h5dir
        self.normalize = normalize
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        
        # Update indices at the end of init
        self.on_epoch_end()

    def __len__(self):
        """Return the number of batches per epoch"""
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        """Generate one batch of data. """
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, y = self.__data_generation(list_IDs_temp)

        return X, y

    def on_epoch_end(self):
        """Updates indexes after each epoch"""
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def _normalize(self, volume):
        """ Normalize data """
        
        vmin = volume.min()
        vmax = volume.max()
        
        volume = (volume - vmin) / (vmax - vmin)
        
        return volume

    def __data_generation(self, list_IDs_temp):
        """Generates data containing batch_size samples"""
        # X : (n_samples, *dim, n_channels)
        # Initialization
        X = np.empty((self.batch_size, *self.dim, self.n_channels))
        y = np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
            h5 = h5py.File(os.path.join(self.h5dir + ID + '.h5'), 'r')
            data = h5['data'][:]
            if self.normalize:
                data = self._normalize(data)
            
            X[i,] = np.expand_dims(data, axis=3)

            # Store class
            y[i] = self.labels[ID]

        return X, keras.utils.to_categorical(y, num_classes=self.n_classes)
