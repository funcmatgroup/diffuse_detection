# Diffuse Detection

Models to try and detect diffuse scattering from diffraction data,
implemented using Keras.

## Quickstart

The file `3D_CNN.py` is designed to train different CNN
models and hyperparameters based on YAML configuration files (see
`basic_cnn.yaml` for an example).

To train a model, you require:
- A directory of hdf5 files containing scattered intensity in (h,k,l)
    - Currently, these are standardised to 100x100x100 voxels
    - Files named 'ID.h5'
- A 'Ground truth' json file denoting the periodicity of any short-range
  correlations (0,1,2 or 3)
    - Formatted as {ID1: 0, ID2: 3 ...}


To train a model/parameter combination (using a suitable Python environement):
```
python 3D_CNN.py -c CONFIG.yaml -w Num_procs -o outdir
```
where outdir is the directory to save a log file and final model.

## Requirements

- json
- h5py
- numpy
- tensorflow
- sklearn


## Files

3D_CNN.py
    Main modular code to test different CNN configurations
data_classes.py
    Class designed for memory-efficient loading of H5 files during keras training
two_class_3D_CNN.py
    Original two-class (yes/no) diffuse scattering model developed during AI3SD-ML4MC summer school
four_class_3D_CNN.py
    Four-class (0,1,2,3) CNN model developed during AI3SD-ML4MC summer school


## Contributors

AI3SD Summer ML4MC Project team:
- Michael High (mh2118@ic.ac.uk)
- Anna Bachs-Herrera (2037458@swansea.ac.uk)
- Dylan Barker (pydsb@leeds.ac.uk)
- Erhan Gulsen (Erhan.Gulsen@nottingham.ac.uk)
- Stefan Kuhn (stefan.kuhn@dmu.ac.uk)
- Feiyu Zhu (fz1a18@soton.ac.uk)

Others:
- James Cumby (james.cumby@ed.ac.uk)