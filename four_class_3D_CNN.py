# Final version of the code generated during the ML4MC
# summer school
#
# Classifies voxelised data into 0, 1, 2 or 3-dimensional
# diffuse scattering

import os
if not os.path.exists('Diffuse.d'):
    os.makedirs('Diffuse.d')

import json
import h5py
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow import estimator

def normalize(volume, min, max):
    volume[volume < min] = min
    volume[volume > max] = max
    volume = (volume - min) / (max - min)
    volume = volume.astype("float32")
    return volume

d = open('truth.json')
labels = json.load(d)

All_labels = ['Temp']
All_data = ['Temp']

for file in os.listdir('Diffuse.d'):
  #All_labels.append(labels[file[:-3]])     #Use this instread of following if statement to train on dimensionality, rather than diffuse scattering yes or no
	
	if labels[file[:-3]] == 0:
		 All_labels.append(0)  
	elif labels[file[:-3]] != 0:
		All_labels.append(1)

	f = h5py.File('Diffuse.d/' + file)
	data = np.array(f['data'])
	data = normalize(data, np.amin(data), np.amax(data))    #Opens and normalises as from Stefan
	datanew = np.zeros((len(data),len(data[0]),len(data[0][0]),1))
	for i in range(len(data)):
		for j in range(len(data[i])):
			for k in range(len(data[i][j])):
				datanew[i][j][k][0]=np.array(data[i][j][k])
	All_data.append(datanew)
 

d.close()

All_labels.remove('Temp')
All_data.remove('Temp')
All_labels = np.array(All_labels)
All_data = np.array(All_data)

#for i in range(len(All_labels)):
#  print("Data: " + ", Label: " + str(All_labels[i]))

ones = 0 
zeros = 0
for label in All_labels:
  if label == 1:
    ones += 1
  if label == 0:
    zeros += 1

print('The total number of disordered samples in the dataset is: ' + str(ones) + '\nThe total number of ordered samples in the dataset is: ' + str(zeros))

split_var = 0.8

train_data, validation_data = np.split(All_data, [int(split_var*len(All_data))])
train_labels, validation_labels = np.split(All_labels, [int(split_var*len(All_labels))])

print('Number of examples for training = ', len(train_data))
print('Number of examples for validation = ', len(validation_data))

# Now for the machine learning bit!
# This is mostly copy and pasted from https://keras.io/examples/vision/3D_image_classification/

train_loader = tf.data.Dataset.from_tensor_slices((All_data, All_labels))
validation_loader = tf.data.Dataset.from_tensor_slices((validation_data, validation_labels))

batch_size = 2

# Augment the on the fly during training.
train_dataset = (
    train_loader.shuffle(len(All_data))
    #.map(train_preprocessing)
    .batch(batch_size)
    .prefetch(2)
)
# Only rescale.
validation_dataset = (
    validation_loader.shuffle(len(validation_data))
    #.map(validation_preprocessing)
    .batch(batch_size)
    .prefetch(2)
)

inputs = keras.Input((100, 100, 100, 1))

x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(inputs)
x = layers.MaxPool3D(pool_size=2)(x)
x = layers.BatchNormalization()(x)

x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(x)
x = layers.MaxPool3D(pool_size=2)(x)
x = layers.BatchNormalization()(x)

x = layers.Conv3D(filters=128, kernel_size=3, activation="relu")(x)
x = layers.MaxPool3D(pool_size=2)(x)
x = layers.BatchNormalization()(x)

x = layers.Conv3D(filters=256, kernel_size=3, activation="relu")(x)
x = layers.MaxPool3D(pool_size=2)(x)
x = layers.BatchNormalization()(x)

x = layers.GlobalAveragePooling3D()(x)
x = layers.Dense(units=512, activation="relu")(x)
x = layers.Dropout(0.3)(x)

outputs = layers.Dense(units=1, activation="sigmoid")(x)

model = keras.Model(inputs, outputs, name="3dcnn")

model.summary()

# Compile the model:

# initial_learning_rate = 0.0001
# lr_schedule = keras.optimizers.schedules.ExponentialDecay(
  #  initial_learning_rate, decay_steps=100000, decay_rate=0.96, staircase=True
#)
# model.compile(
  #  loss="binary_crossentropy",
  #  optimizer=keras.optimizers.Adam(learning_rate=lr_schedule),
  #  metrics=["acc"],
#)

model.compile(
    loss="categorical_crossentropy",
    optimizer=keras.optimizers.Adam(lr=0.01),#lr_schedule),
    metrics=["acc"],
)

'''
# Define callbacks.
checkpoint_cb = keras.callbacks.ModelCheckpoint(
    "3d_image_classification.h5", save_best_only=True
)
early_stopping_cb = keras.callbacks.EarlyStopping(monitor="val_acc", patience=15)
'''
# Train the model, doing validation at the end of each epoch
epochs = 10
model.fit(
    	train_dataset,
    #validation_data=validation_dataset,
	steps_per_epoch=20,
    	epochs=epochs,
    	shuffle=True,
    	verbose=1,
)

test_loss, test_acc = model.evaluate(validation_data,  validation_labels, verbose=1) 

print('Test accuracy:', test_acc)







