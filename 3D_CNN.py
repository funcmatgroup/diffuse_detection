# General 3D CNN to detect diffuse scattering

import json
import h5py
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from sklearn.model_selection import train_test_split

from tensorflow.keras.callbacks import CSVLogger

import os
import yaml

from data_classes import DataGenerator

split_var = 0.8

def normalise(volume, min, max):
    volume[volume < min] = min
    volume[volume > max] = max
    volume = (volume - min) / (max - min)
    volume = volume.astype("float32")
    return volume


def read_data(h5file, normalise=True):
    """ Read a hdf5 file of voxelised diffraction intensities.

    Parameters
    ----------

    h5file : str
        Name of hdf5 file to read
    normalise : bool, optional
        Whether to normalise data before return (default True)

    Returns
    -------

    data : np.array
        3D array of intensity data in h,k,l coordinates

    """

    if not os.path.exists(h5file):
        raise FileNotFoundError('{h5file} does not exist.')

    h5 = h5py.File(h5file)

    data = h5['data'][:]

    if normalise:
        vol_min = data.min()
        vol_max = data.max()
        data = normalize(data, vol_min, vol_max)
    
    return data


def read_all_data(groundtruth, h5dir = './'):
    """ Read all data and labels based on a groundtruth file.

    Parameters
    ----------

    groundtruth : str
        Path to json file containing ground truth labels in the format
        {H5file basename : diffuse_dimensionality}
    h5dir : str, optional
        Path to h5 data files (default '.')

    Returns
    -------
    
    data : np.array
        4-dimensional array of h,k,l data
    labels : np.array
        Array of diffuse dimensionality labels
    """

    with open(groundtruth) as f:
        truth = json.load(f)


    data = []
    labels = []

    for structure in truth:
        path = os.path.join(h5dir, structure + '.h5')
        
        try:
            data.append(read_data(path))
        except FileNotFoundError:
            print(f'No data file found for {structure} - skipping') 

        labels.append(truth[structure])

    data = np.array(data)
    labels = np.array(labels)

    return data, labels


def all_data_generator(groundtruth, h5dir='.'):
    """
    Generator to return data and groundtruth labels

    """

    with open(groundtruth) as f:
        truth = json.load(f)


    for structure in truth:
        path = os.path.join(h5dir, structure + '.h5')
        
        try:
            data = read_data(path)
        except FileNotFoundError:
            print(f'No data file found for {structure} - skipping') 

        label = truth[structure]

        yield data, label


def read_labels_ids(truth_file, h5dir, omit_labels = []):
    """ Read file IDs and groundtruth from json file and check file exists. """
    
    IDs = []
    labels = []
    
    with open(truth_file) as f:
        truth = json.load(f)

    for ID in truth:
        if truth[ID] in omit_labels:
            continue
        if os.path.exists(os.path.join(h5dir, ID + '.h5')):
            IDs.append(ID)
            labels.append(truth[ID])
        
    #IDs = list(truth.keys())
    #labels = list(truth.values())

    return IDs, labels
    

def reindex_label_dict(label_dict):
    """ Reindex labels to 0,1,...,n to correct for missing values """

    unique_labels = sorted(list(set(label_dict.values())))

    new_dict = {}

    for idx in label_dict:
        new_dict[idx] = unique_labels.index(label_dict[idx])

    return new_dict
    

def partition_train_test(IDs, labels, test_size=0.2):
    """ Generate dictionaries of IDs and labels split in to train/validation sets """
    
    X_train, X_test, y_train, y_test = train_test_split(IDs, labels, test_size=test_size, shuffle=True)
    
    partition = {}
    partition['train'] = X_train
    partition['validation'] = X_test
    
    label_dict = {}
    for i, idx in enumerate(IDs):
        label_dict[idx] = labels[i]
        
    return partition, label_dict
    
def basic_3d_cnn(categories = 4, final_dropout = 0.3, dense_size = 512):
    """ Basic 3D CNN model definition """
    
    inputs = keras.Input((100, 100, 100, 1))

    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(inputs)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=128, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=256, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.GlobalAveragePooling3D()(x)
    x = layers.Dense(units=dense_size, activation="relu")(x)
    x = layers.Dropout(final_dropout)(x)

    outputs = layers.Dense(units=categories, activation="sigmoid")(x)

    model = keras.Model(inputs, outputs, name="3dcnn")
    
    return model

def global_max_pool(categories = 4, final_dropout = 0.3, dense_size = 512):
    """ Similar to basic_3d_cnn model but with GlobalMaxPooling3D rather than GlobalAveragePooling3D """
    
    inputs = keras.Input((100, 100, 100, 1))

    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(inputs)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=128, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=256, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.GlobalMaxPooling3D()(x)
    x = layers.Dense(units=dense_size, activation="relu")(x)
    x = layers.Dropout(final_dropout)(x)

    outputs = layers.Dense(units=categories, activation="sigmoid")(x)

    model = keras.Model(inputs, outputs, name="3dcnn")
    
    return model


def cnn_no_norm(categories = 4, final_dropout = 0.3, dense_size = 512):
    """ Basic CNN model without BatchNorm layers. """

    inputs = keras.Input((100, 100, 100, 1))

    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(inputs)
    x = layers.MaxPool3D(pool_size=2)(x)
    
    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    
    x = layers.Conv3D(filters=128, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    
    x = layers.Conv3D(filters=256, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    
    x = layers.GlobalAveragePooling3D()(x)
    x = layers.Dense(units=dense_size, activation="relu")(x)
    x = layers.Dropout(final_dropout)(x)

    outputs = layers.Dense(units=categories, activation="sigmoid")(x)

    model = keras.Model(inputs, outputs, name="3dcnn")
    
    return model

def make_custom_cnn(filters = [64,64,128,256],
                    kernels = [3, 3, 3, 3],
                    batchnorm = True,
                    dense_size = 512,
                    dropout = 0.3,
                    categories = 4,
                    ):
    """ Generate custom cnn based on Conv3D/MaxPool3D/BatchNorm blocks """
    
    inputs = keras.Input((100, 100, 100, 1))
    x = inputs

    assert len(filters) == len(kernels)
    
    for i, filt in enumerate(filters):
        x = _make_cnn_block(x, filt, kernels[i], batchnorm = batchnorm)

    x = _make_dense_dropout(x, dense_size = dense_size, dropout = dropout)

    outputs = layers.Dense(units = categories, activation="sigmoid")(x)

    model = keras.Model(inputs, outputs, name="3dcnn")

    return model

def _make_cnn_block(x, filters, kernel_size, batchnorm = True):
    """ Generate a single Conv3D/MaxPool3D/BatchNorm block of varying size """
    
    y = layers.Conv3D(filters=filters, kernel_size=kernel_size, activation='relu')(x)
    y = layers.MaxPool3D(pool_size=2)(y)
    if batchnorm:
        y = layers.BatchNormalization()(y)
    return y

def _make_dense_dropout(x, dense_size=512, dropout=0.3):
    """ Generate final dense/dropout block """

    y = layers.GlobalAveragePooling3D()(x)
    y = layers.Dense(units=dense_size, activation="relu")(y)
    y = layers.Dropout(dropout)(y)

    return y
    

def main(ground_truth,
         h5dir,
         output_dir = './model/',
         test_size=0.2,
         omit_labels = [],
         batch_size = 8,
         normalize = True,
         epochs = 20,
         workers = 8,
         architecture = 'basic_3d_cnn',
         initial_lr = 0.0001,
         decay = 0.96,
         final_dropout = 0.3,
         dense_size = 512,
         ):

    IDs, labels = read_labels_ids(ground_truth, h5dir, omit_labels = omit_labels)

    partition, labels = partition_train_test(IDs, labels, test_size=test_size)

    if len(omit_labels) > 0:
        labels = reindex_label_dict(labels)
    

    
    params = {'dim': (100,100,100),
              'batch_size': batch_size,
              'n_classes': 4 - len(omit_labels),
              'n_channels': 1,
              'shuffle': True,
              'normalize': normalize,
              'h5dir': h5dir,
             }
    
    
    training_generator = DataGenerator(partition['train'], labels, **params)
    validation_generator = DataGenerator(partition['validation'], labels, **params)

    # Configure gpu memory growth
    # For tf v2.0.0:
    gpus = tf.config.experimental.list_physical_devices('GPU')
    # For up to date tf
    #gpus = tf.config.list_physical_devices('GPU')
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)

    
    
    if isinstance(architecture, str):
        if architecture in globals():
            # Test if model is hard-coded
            model = eval(architecture)(categories = params['n_classes'],
                                               final_dropout = final_dropout,
                                               dense_size = dense_size)
    elif isinstance(architecture, dict):
        
        # Construct model from scratch
        model = make_custom_cnn(filters = architecture['filters'],
                                kernels = architecture['kernels'],
                                batchnorm = architecture['batchnorm'],
                                dense_size = dense_size,
                                dropout = final_dropout,
                                categories = params['n_classes'],
                                )

    else:
        print('Unknown model architecture - stopping')
        return

    print(model.summary())

    # Normalize decay to number of steps in an epoch
    steps_per_epoch = int(len(partition['train']) / batch_size)
            
    lr_schedule = keras.optimizers.schedules.ExponentialDecay(
                    initial_lr, decay_steps=steps_per_epoch, decay_rate=decay, staircase=False
                    )


    model.compile(
        loss="categorical_crossentropy",
        optimizer=keras.optimizers.Adam(learning_rate=lr_schedule),
        metrics=["acc"],
    )

    if workers > 1:
        multiproc = True
    else:
        multiproc = False

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    csv_logger = CSVLogger(os.path.join(output_dir, 'log.csv'), append=True, separator=',')


    # Train the model, doing validation at the end of each epoch
    model.fit(x = training_generator,
              validation_data = validation_generator,
              use_multiprocessing = multiproc,
              workers = workers,
              epochs=epochs,
              verbose=2,
              callbacks=[csv_logger],
             )

    model.save(output_dir)

if __name__ == "__main__":
    
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('-c', '--config', type=str,
                        help = 'YAML configuration file to use for model training')

    parser.add_argument('-w', '--workers', type=int, default = 1, 
                        help = 'Number of parallel processes')

    parser.add_argument('-o', '--outdir', type=str, default = '',
                        help = 'Directory to save model and log file')

    args = parser.parse_args()

    if args.outdir == '':
        outdir = os.path.splitext(args.config)[0]
    else:
        outdir = args.outdir

    with open(args.config, 'r') as f:
        config = yaml.safe_load(f)

    main(**config, output_dir = outdir, workers = args.workers)

    #main(ground_truth = 'truth.json',
    #     h5dir = '/exports/eddie/scratch/jcumby/diffuse/',
    #     omit_labels = [3],
    #     workers=1,
    #     test_size=0.2,
    #    )
